/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.SNMP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.event.ResponseListener;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;

/**
 *
 * @author Mastro
 */
public class SNMPController {
    
    Snmp snmp;
    String address;
    
    public SNMPController(String address){
        
        this.address = address;
        
        try{
            start();
        }catch(IOException e){
            throw new RuntimeException(e);
        }
        
    }
    
    private void start() throws IOException {
    
        TransportMapping transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
	// Do not forget this line!
        transport.listen();
    }
    
    public void Stop() throws IOException{
        
        this.snmp.close();
    }

    public String getAsString(OID oid) throws IOException {
	ResponseEvent event = get(new OID[]{oid});
	return event.getResponse().get(0).getVariable().toString();
    }
    
    public void getAsString(OID oids,ResponseListener listener) {
	try {
            snmp.send(getPDU(new OID[]{oids}), getTarget(),null, listener);
	} catch (IOException e) {
			throw new RuntimeException(e);
	}
    }
    
    private PDU getPDU(OID oids[]) {
	PDU pdu = new PDU();
        
            for (OID oid : oids) {
		pdu.add(new VariableBinding(oid));
            }
	 	   
	pdu.setType(PDU.GET);
	return pdu;
    }
    
    public ResponseEvent get(OID oids[]) throws IOException {
	PDU pdu = new PDU();
 	for (OID oid : oids) {
 	     pdu.add(new VariableBinding(oid));
 	}
 	pdu.setType(PDU.GET);
 	ResponseEvent event = snmp.send(pdu, getTarget(), null);
	if(event != null) {
             return event;
	}
	throw new RuntimeException("GET timed out");
    }
    
    private Target getTarget() {
	Address targetAddress = GenericAddress.parse(address);
	CommunityTarget target = new CommunityTarget();
	target.setCommunity(new OctetString("public"));
	target.setAddress(targetAddress);
	target.setRetries(2);
	target.setTimeout(1500);
	target.setVersion(SnmpConstants.version2c);
	return target;
    }
    
    public List<List<String>> getTableAsStrings(OID[] oids) {
		TableUtils tUtils = new TableUtils(snmp, new DefaultPDUFactory());
		
		@SuppressWarnings("unchecked") 
			List<TableEvent> events = tUtils.getTable(getTarget(), oids, null, null);
		
		List<List<String>> list = new ArrayList();
                events.stream().map((event) -> {
                    if(event.isError()) {
                        throw new RuntimeException(event.getErrorMessage());
                    }
            return event;
        }).forEachOrdered((event) -> {
            List<String> strList = new ArrayList();
            list.add(strList);
            for(VariableBinding vb: event.getColumns()) {
                strList.add(vb.getVariable().toString());
            }
        });
		return list;
    }
    
    public static String extractSingleString(ResponseEvent event) {
	return event.getResponse().get(0).getVariable().toString();
    }
    
}
