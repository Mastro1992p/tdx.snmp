/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.SNMP.Trap;

import java.io.IOException;
import java.util.Date;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

/**
 *
 * @author Mastro
 */
public class TrapSender {
    
  public static String  Community = "public";
  public static String  TrapOid = ".1.3.6.1.2.1.1.6";                         
  public static String  IpAddress = "127.0.0.1";
  public static int     Port = 162;
  

  public void sendSnmpV1Trap(){
    try{
      //Create Transport Mapping
      TransportMapping transport = new DefaultUdpTransportMapping();
      transport.listen();

      //Create Target 
      CommunityTarget comtarget = new CommunityTarget();
      comtarget.setCommunity(new OctetString(Community));
      comtarget.setVersion(SnmpConstants.version1);
      comtarget.setAddress(new UdpAddress(IpAddress + "/" + Port));
      comtarget.setRetries(2);
      comtarget.setTimeout(5000);

      //Create PDU for V1
      PDUv1 pdu = new PDUv1();
      pdu.setType(PDU.V1TRAP);
      pdu.setEnterprise(new OID(TrapOid));
      pdu.setGenericTrap(PDUv1.ENTERPRISE_SPECIFIC);
      pdu.setSpecificTrap(1);
      pdu.setAgentAddress(new IpAddress(IpAddress));

      //Send the PDU
      Snmp snmp = new Snmp(transport);
      System.out.println("Sending V1 Trap to " + IpAddress + " on Port " + Port);
      snmp.send(pdu, comtarget);
      snmp.close();
    }catch (Exception e){
      System.err.println("Error in Sending V1 Trap to " + IpAddress + " on Port " + Port);
      System.err.println("Exception Message = " + e.getMessage());
    }
  }
  
  public void sendSnmpV2Trap(){
    try{
      //Create Transport Mapping
      TransportMapping transport = new DefaultUdpTransportMapping();
      transport.listen();

      //Create Target 
      CommunityTarget comtarget = new CommunityTarget();
      comtarget.setCommunity(new OctetString(Community));
      comtarget.setVersion(SnmpConstants.version2c);
      comtarget.setAddress(new UdpAddress(IpAddress + "/" + Port));
      comtarget.setRetries(2);
      comtarget.setTimeout(5000);

      //Create PDU for V2
      PDU pdu = new PDU();
      
      // need to specify the system up time
      pdu.add(new VariableBinding(SnmpConstants.sysUpTime, new OctetString(new Date().toString())));
      pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(TrapOid)));
      pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress, new IpAddress(IpAddress)));

      // variable binding for Enterprise Specific objects, Severity (should be defined in MIB file)
      pdu.add(new VariableBinding(new OID(TrapOid), new OctetString("Major"))); 
      pdu.setType(PDU.NOTIFICATION);
      
      //Send the PDU
      Snmp snmp = new Snmp(transport);
      System.out.println("Sending V2 Trap to " + IpAddress + " on Port " + Port);
      snmp.send(pdu, comtarget);
      snmp.close();
    }catch (Exception e){
      System.err.println("Error in Sending V2 Trap to " + IpAddress + " on Port " + Port);
      System.err.println("Exception Message = " + e.getMessage());
    }
  }



  public static String getCommunity() {
      return Community;
  }

  public static String getTrapOid() {
      return TrapOid;
  }
    
  public static String getIpAddress() {
      return IpAddress;
  }

  public static int getPort() {
      return Port;
  }

    public static void setCommunity(String Community) {
        TrapSender.Community = Community;
    }

    public static void setTrapOid(String TrapOid) {
        TrapSender.TrapOid = TrapOid;
    }

    public static void setIpAddress(String IpAddress) {
        TrapSender.IpAddress = IpAddress;
    }

    public static void setPort(int Port) {
        TrapSender.Port = Port;
    }

    public TrapSender() {
    }
    
    
}
