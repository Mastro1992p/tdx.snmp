/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tdx.SNMP.config;

import java.util.Properties;

/**
 *
 * @author Mastro
 */
public class SNMPControllerConfiguration {
    
    public static String Address = "DefaultAddress";
    public static String MainPort = "MainPort";
    public static String TrapPort = "TrapPort";
    public static int MainPortI = 161;
    public static int TrapPortI = 162;
    

    private SNMPControllerConfiguration(Properties properties) {
        
        Address = properties.getProperty(Address, "localhost:161");
        MainPort = properties.getProperty(MainPort, "161");
        TrapPort = properties.getProperty(TrapPort, "TrapPort");
        MainPortI = Integer.parseInt(MainPort);
        TrapPortI = Integer.parseInt(TrapPort);
        
    }
    
    
    
    public static String getAddress() {
        return Address;
    }

    public static void setAddress(String Address) {
        SNMPControllerConfiguration.Address = Address;
    }

    public static String getMainPort() {
        return MainPort;
    }

    public static void setMainPort(String MainPort) {
        SNMPControllerConfiguration.MainPort = MainPort;
    }

    public static String getTrapPort() {
        return TrapPort;
    }

    public static void setTrapPort(String TrapPort) {
        SNMPControllerConfiguration.TrapPort = TrapPort;
    }

    public static int getMainPortI() {
        return MainPortI;
    }

    public static void setMainPortI(int MainPortI) {
        SNMPControllerConfiguration.MainPortI = MainPortI;
    }

    public static int getTrapPortI() {
        return TrapPortI;
    }

    public static void setTrapPortI(int TrapPortI) {
        SNMPControllerConfiguration.TrapPortI = TrapPortI;
    }
       
}
